package main

import "fmt"

func f() error {
	return fmt.Errorf("expected error")
}

func main() {
	if f().Error() != "expected error" {
		panic("unexpected error!")
	}
	fmt.Println("All good :)")
}