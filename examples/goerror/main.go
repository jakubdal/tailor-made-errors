package main

import (
	"fmt"
	"github.com/go-chi/chi"
	"github.com/pkg/errors"
	"gitlab.com/jakubdal/goerror"
	"net/http"
)

// FUNC_START OMIT
func handler(statusCode int) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		err := erroringFunc(statusCode)
		w.WriteHeader(goerror.HTTPStatusCode(err))
		w.Write([]byte(err.Error()))
	}
}

func erroringFunc(statusCode int) error {
	err := errors.New("some cause")
	return goerror.WithHTTPStatusCode(err, statusCode)
}

func getPrintCode(path string) {
	resp, _ := http.Get("http://localhost:8080/" + path)
	fmt.Println(path, resp.StatusCode)
}
// FUNC_END OMIT

// RUN_START OMIT
func main() {
	r := chi.NewRouter()
	r.Get("/400", handler(400))
	r.Get("/404", handler(404))
	r.Get("/500", handler(500))
	go http.ListenAndServe(":8080", r)

	getPrintCode("400")
	getPrintCode("404")
	getPrintCode("500")
}
// RUN_END OMIT