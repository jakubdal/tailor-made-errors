package main

import "fmt"

const EOF Error = "EOF"

type Error string

func (e Error) Error() string {
	return string(e)
}

func f() error {
	return EOF
}

func main() {
	if err := f(); err != nil {
		fmt.Println("Yay!", err.Error())
	}
}