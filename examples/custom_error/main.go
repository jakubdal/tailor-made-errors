package main

import "fmt"

type customError struct{}

func (err *customError) Error() string {
	return "wat"
}

func f() *customError {
	return nil
}

func g() error {
	return f()
}

func main() {
	if err := g(); err != nil {
		fmt.Println(err.Error())
	}
}
