package main

import (
	"fmt"
	"github.com/pkg/errors"
)

func f() error {
	err := errors.New("whoops")
	return errors.WithStack(err)
}

func g() error {
	return f()
}

func main() {
	fmt.Printf("%+v", g())
}
