package main

const EOF = Error(func() string {return "EOF" })

type Error func() string

func (f Error) Error() string {
	return f()
}

func main() {}
