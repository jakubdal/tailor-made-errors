package main

import "fmt"

// START OMIT
func panickingFunc() { panic("\tExpected panic!") }

func fallbackFunc() { fmt.Println("\tIt works!") }

func withFallback(funcs ...func()) {
	if len(funcs) == 0 {
		return
	}
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("\tFallback...")
			withFallback(funcs[1:]...)
		}
	}()
	funcs[0]()
}

func main() {
	fmt.Println("call 1")
	withFallback(fallbackFunc)
	fmt.Println("call 2")
	withFallback(panickingFunc, panickingFunc, panickingFunc, fallbackFunc)
}
// END OMIT