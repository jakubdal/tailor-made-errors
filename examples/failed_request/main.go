package main

import (
	"log"
	"net/http"
)

func main() {
	_, err := http.Get("http://bytedance.com/")
	if err != nil {
		log.Fatalf("http.Get: %v", err)
	}
	panic("This was not supposed to happen...")
}
