package main

import (
	"fmt"
	"github.com/go-chi/chi"
	"net/http"
	"time"
)

// START OMIT
func main () {
	requests := 0
	r := chi.NewRouter()
	r.Get("/", func(w http.ResponseWriter, r *http.Request) {
		requests++
		fmt.Println(requests)
		if requests%2 == 0 {
			panic("Failed request!")
		}
		fmt.Println("Succeeded request!")
	})
	go http.ListenAndServe(":8080", r)

	for {
		http.Get("http://localhost:8080/")
		time.Sleep(time.Second * 2)
	}
}
// END OMIT