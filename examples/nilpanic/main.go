package main

import (
	"fmt"
	"io"
	"time"
)
// START OMIT
var eof = io.EOF

func main() {
	go func() {
		time.Sleep(time.Second*3)
		io.EOF = nil
	}()

	for {
		fmt.Println(eof == io.EOF, nil == io.EOF)
		time.Sleep(time.Second)
	}
}
// END OMIT