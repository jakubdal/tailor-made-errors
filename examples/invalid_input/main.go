package main

import (
	"encoding/json"
	"fmt"
)

type nestedNumber struct{ Number int `json:"number"` }

func getNumber(body []byte) (int, error) {
	var str nestedNumber
	err := json.Unmarshal(body, &str)
	if err != nil {
		return 0, err
	}
	return str.Number, nil
}

func main() {
	number := "7"
	body := fmt.Sprintf("{\"number\":%v}", number)
	num, err := getNumber([]byte(body))
	fmt.Printf("num:%v\nerr:%v\n", num, err)
}
